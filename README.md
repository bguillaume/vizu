Rendu 1 : Visualisation

Les crimes aux Etats Unis

Notre page représente sur la carte des Etats Unis les données par états des variables suivantes :

* La population urbaine
* Le nombre de viols
* Le nombre de meurtres
* Le nombre d’agression


La population urbaine semble souvent expliquer les autres variables donc nous avons essayé de visualiser les corrélations.

Donc sur chaque graphique, la population urbaine donne le rayon de chaque état alors que les couleurs représentent :

* pour l’onglet meurtre: Les états en rouge représentent des états avec de nombreux meurtres alors que ceux en bleu avec peu de meurtres.
* pour l’onglet agression: Les états en verts représentent des états avec de nombreuses agressions alors que ceux en jaune représentent les états avec peu d’agressions.
* Pour l’onglet viols: Les états en noirs représentent des états avec de nombreux viols tandis que ceux en blancs représentent peu de viols.